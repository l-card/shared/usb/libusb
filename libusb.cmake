# Файл для включения в проект на CMAKE.
# После включения будут установлены следующие перменные:
#   LIBUSB_HEADERS - используемые заголовочные файлы
#   LIBUSB_SOURCES - используемые файлы исходных кодов
#   LIBUSB_INCLUDE_DIRS - директории включения заголовков
#   LIBUSB_LIBS    - используемые библиотеки

cmake_policy(PUSH)

cmake_minimum_required(VERSION 2.8.12)




set(LIBUSB_DIR     ${CMAKE_CURRENT_LIST_DIR})
set(LIBUSB_DIR_SRC ${LIBUSB_DIR}/libusb-1.0)

set(LIBUSB_INCLUDE_DIRS ${LIBUSB_DIR})

set(LIBUSB_SOURCES 
    ${LIBUSB_DIR_SRC}/core.c
    ${LIBUSB_DIR_SRC}/descriptor.c
    ${LIBUSB_DIR_SRC}/hotplug.c
    ${LIBUSB_DIR_SRC}/io.c
    ${LIBUSB_DIR_SRC}/strerror.c
    ${LIBUSB_DIR_SRC}/sync.c
)

set(LIBUSB_HEADERS
    ${LIBUSB_DIR_SRC}/libusb.h
    ${LIBUSB_DIR_SRC}/libusbi.h
    ${LIBUSB_DIR_SRC}/hotplug.h
    ${LIBUSB_DIR_SRC}/version.h
    ${LIBUSB_DIR_SRC}/version_nano.h
)


if(WIN32)
    include(CheckStructHasMember)
    check_struct_has_member("struct timespec" tv_sec time.h HAVE_STRUCT_TIMESPEC LANGUAGE C)
    if(HAVE_STRUCT_TIMESPEC)
        add_definitions(-DHAVE_STRUCT_TIMESPEC)
    endif(HAVE_STRUCT_TIMESPEC)
    set(SOURCES ${SOURCES}
        ${LIBUSB_DIR_SRC}/os/poll_windows.c
        ${LIBUSB_DIR_SRC}/os/threads_windows.c
        ${LIBUSB_DIR_SRC}/os/windows_usb.c
    )
    set(HEADERS ${HEADERS}
        ${LIBUSB_DIR_SRC}/os/poll_windows.h
        ${LIBUSB_DIR_SRC}/os/threads_windows.h
        ${LIBUSB_DIR_SRC}/os/windows_common.h
    )
else(WIN32)
    message(FATAL_ERROR "unsupported os")
endif(WIN32)



if(MSVC)
    set(LIBUSB_INCLUDE_DIRS ${LIBUSB_INCLUDE_DIRS} ${LIBUSB_DIR_SRC}/msvc)

    #В зависимости от версии msvc файлы errno.h,stdint.h,inttypes.h могут присутсвовать
    #или отсутствовать. При этом файлы из libusb могут конфликтовать с файлами из msvc.
    #Поэтому проверяем каждый из этих файлов, и прописываем до него путь в поиске include,
    #только в случае, если он не найден
    include(CheckIncludeFile)
    check_include_file(errno.h HAVE_ERRNO)
    if (NOT ${HAVE_ERRNO})
        set(LIBUSB_INCLUDE_DIRS ${LIBUSB_INCLUDE_DIRS} ${LIBUSB_DIR_SRC}/errno)
    endif (NOT ${HAVE_ERRNO})

    check_include_file(stdint.h HAVE_STDINT)
    if (NOT ${HAVE_STDINT})
        set(LIBUSB_INCLUDE_DIRS ${LIBUSB_INCLUDE_DIRS} ${LIBUSB_DIR_SRC}/stdint)
    endif (NOT ${HAVE_STDINT})

    check_include_file(inttypes.h HAVE_INTTYPES)
    if (NOT ${HAVE_INTTYPES})
        set(LIBUSB_INCLUDE_DIRS ${LIBUSB_INCLUDE_DIRS} ${LIBUSB_DIR_SRC}/inttypes)
    endif (NOT ${HAVE_INTTYPES})

    set(LIBUSB_HEADERS ${LIBUSB_HEADERS}
        ${LIBUSB_DIR_SRC}/msvc/config.h
        ${LIBUSB_DIR_SRC}/msvc/missing.h
        ${LIBUSB_DIR_SRC}/msvc/errno/errno.h
        ${LIBUSB_DIR_SRC}/msvc/inttypes/inttypes.h
        ${LIBUSB_DIR_SRC}/msvc/stdint/stdint.h
    )
else(MSVC)
    message(FATAL_ERROR "unsupported compiler")
endif(MSVC)


cmake_policy(POP)
